import numpy as np
import random
import pyphen

def LoadIntoList(fileName):
  with open(fileName) as raw:
    lines = [line.strip().lower() for line in raw]
  return lines

def getSyllables(rawData):
  syllableList = []
  dic = pyphen.Pyphen(lang='en')
  for line in rawData:
    words = line.split()
    for word in words:
      hyphenated = dic.inserted(word)
      for item in hyphenated.split("-"):
        syllableList.append(item)
  return syllableList

def makePairs(rawData):
  for line in rawData:
    splitLines = line.split()
    for word in splitLines:
      word = word.lower()
      word = word.split("'")[0]
      for i in range(len(word)-1):
        if i > 0:
          key = word[i-1] + word[i]
          value = word[i+1]
          yield(key,value)
        key = word[i]
        value = word[i+1]
        yield (key, value)

def CreateDictionary(pairs):
  charDict = {}
  for char1, char2 in pairs:
    if char1 in charDict.keys():
      charDict[char1].append(char2)
    else:
      charDict[char1] = [char2]
  return charDict

def GenerateWord(numberOfWords, wordSize, dictionary):
  namesList = []
  for j in range(numberOfWords):
    chain = random.choice(dictionary)
    for i in range(wordSize):
      randomChar = random.choice(dictionary)
      chain = chain + randomChar
    namesList.append(chain)
  return namesList

if __name__ == "__main__":
  wordList = LoadIntoList('orcish.txt')
  charDict = getSyllables(wordList)
  # pairs = makePairs(wordList)
  # charDict = CreateDictionary(pairs)
  townNameList = GenerateWord(5,1,charDict)
  for town in townNameList:
    print(town)