#!usr/bin/env python3

import requests
from bs4 import BeautifulSoup
import numpy as np

page = requests.get("http://www.angelfire.com/ia/orcishnations/englishorcish.html")
soup = BeautifulSoup(page.text, 'lxml')


wordsList = []

trs = soup.find_all("tr")
for tr in trs[1:]:
	tds = tr.find_all("td")
	raw = tds[1]
	proccessed = raw.renderContents()
	trimmed = proccessed.strip().decode("utf-8")
	wordsList.append(trimmed)

f = open("orcish.txt",'w').close()
f = open("orcish.txt", "a")
for item in wordsList:
	split = item.split(",")
	split = split[0].split("-")
	split = split[0].split("(")

	f.write(split[0] + "\n")
f.close()