#!/usr/bin/env python3

import requests
from bs4 import BeautifulSoup
import numpy as np

page = requests.get("https://en.wikipedia.org/wiki/List_of_towns_in_England")
soup = BeautifulSoup(page.text, 'html.parser')

tables = soup.find_all('table',class_ = 'wikitable sortable')

NamesList = []

for table in tables:
	tds = table.find_all('td')
	for td in tds:
		ays = td.find_all('a')
		for a in ays:
			name = a.get('title')
			if type(name) is not type(None):
				NamesList.append(name)

f = open("output.txt",'w').close()
f = open("output.txt", "a")
for item in NamesList:
	split = item.split(",")
	split = split[0].split("-")
	split = split[0].split("(")

	f.write(split[0] + "\n")
f.close()